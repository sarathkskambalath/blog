<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Blog;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $BlogData = []; 

        if($request->input('user_id')){ 
            $UserId   = $request->input('user_id');
            $BlogData = Blog::where('user_id', '=' ,$UserId)->paginate(15); 
        }else{
            $BlogData = Blog::paginate(15);
        }

        return response()->json($BlogData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //return response()->json($request->input('userdata'));  
 
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'images' => 'required|max:100000', 
        ]); 

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->messages()]);
        }

         
        //GETTING LOGED IN USER DATA
        $userData = json_decode($request->input('userdata')); 

        //dd($userData->id);   
  
        // Store the Blog post...
        $BlogData               = new Blog();
        $BlogData->title        = $request->input('title','');
        $BlogData->description  = $request->input('description',''); 
        $BlogData->tags         = 'Blog'; 
        $BlogData->images       = json_encode(''); 
        $BlogData->user_id      = $userData->id; 
        $BlogData->save();

        //UPLOAD IMAGES
        $Extension      = '';
        $imagesExploded = explode(',', $request->input('images') );
        $decodedImg     = base64_decode($imagesExploded[1]);

        if(str_contains($imagesExploded[0], 'jpeg')){
            $Extension = 'jpg';
        }elseif(str_contains($imagesExploded[0], 'png')){
            $Extension = 'png';
        }

        $filename = str_random().'.'.$Extension;

        $path     = public_path().'/images/blog/'.$filename;

        file_put_contents($path, $decodedImg);

        //UPDATE IMAGE
        $BlogData->images = json_encode($filename);
        $BlogData->update();




        // $images   = array();
        // if($files = $request->file('images')){
        //     foreach($files as $key=>$file){
        //         $name=$file->getClientOriginalName()."_".$key."_".$BlogData->id;
        //         $file->move(public_path().'/images/',$name);
        //         $images[]=$name;
        //     }

        //     //UPDATE IMAGE
        //     $BlogData->images = json_encode($images);
        //     $BlogData->update();
        // }

        return response()->json($BlogData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $BlogData = Blog::find($id);
        return response()->json($BlogData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
