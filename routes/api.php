<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request->user();
});

Route::prefix('/usercurd')->group(function () {  
    Route::get('/', 'UserController@index'); 
    Route::post('/store', 'UserController@store');
    Route::post('/update/{id?}', 'UserController@update');
    Route::get('/show/{id?}', 'UserController@show');
    Route::delete('/delete/{id?}', 'UserController@destroy');
    Route::post('/checklogin', 'UserController@checklogin');
}); 

Route::prefix('/blogcurd')->group(function () {  
    Route::get('/', 'BlogsController@index'); 
    Route::post('/store', 'BlogsController@store');
    Route::post('/update/{id?}', 'BlogsController@update');
    Route::get('/show/{id?}', 'BlogsController@show');
    Route::delete('/delete/{id?}', 'BlogsController@destroy');  
}); 


 