<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $UserData = User::paginate();
        return response()->json($UserData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        //return response()->json($request->all());  
          
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]); 

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->messages()]);
        }
 

        // Store the USER post...
        $UserData = new User();
        $UserData->name = $request->input('name','');
        $UserData->email = $request->input('email','');
        $UserData->mobile = $request->input('mobile','');
        $UserData->password = Hash::make($request->input('password',''));
        $UserData->save();

        return response()->json($UserData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $UserData = User::find($id);
        return response()->json($UserData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 

        // Store the USER post...
        $UserData = User::find($id);
        $UserData->name = $request->input('name')? $request->input('name'):'';
        $UserData->email = $request->input('email')? $request->input('email'):'';
        $UserData->mobile = $request->input('mobile')? $request->input('mobile'):'';

        if($request->input('password')){
            $UserData->password = Hash::make($request->input('password'));
        }

        $UserData->save(); 

        if($request->input('images') != 'undefined' ){
            
            //UPLOAD IMAGES
            $Extension      = '';
            $imagesExploded = explode(',', $request->input('images') ); 

            if(!empty($imagesExploded[1])){
               
                $decodedImg     = base64_decode($imagesExploded[1]);

                if(str_contains($imagesExploded[0], 'jpeg')){
                    $Extension = 'jpg';
                }elseif(str_contains($imagesExploded[0], 'png')){
                    $Extension = 'png';
                }

                $filename = str_random().'.'.$Extension;

                $path     = public_path().'/images/profile/'.$filename;

                file_put_contents($path, $decodedImg);

                //UPDATE IMAGE
                $UserData->images = $filename;
                $UserData->update();
                
            }
        }
 

        return response()->json($UserData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) 
    {
        $UserData = User::find($id);
        
        if(User::destroy($id)){
            return response()->json($UserData);
        }
    }

    /*
     * LOGIN FUNCTION
     * 
     */
    public function checklogin(Request $request)
    {   
        $inputAuth         =  $request->input('auth');
         
        $inputAuth         = base64_decode( $inputAuth );
        $Authorization     = explode(":",$inputAuth);
 
        $username          = $Authorization[0] ? $Authorization[0] : '';
        $password          = $Authorization[1] ? $Authorization[1] : '';
 
         
        // Store the USER post...
        $UserData =  User::where([  
                                    ['email', '=' , $username],
                                 ])->first();
                          
        if($UserData != null and $UserData->count() >=1 and (Hash::check($password, $UserData->password)) !== false ){ 
            return response()->json($UserData);
        }else{
            return response()->json(["errors" => 'Your Credential has not available.']);
        }                         
                                 

    }
     
}
