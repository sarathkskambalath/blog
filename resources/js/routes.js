
import Login  from './components/auth/Login.vue';
import Register  from './components/auth/Register.vue';
import Profile  from './components/auth/Profile.vue';
import Home  from './components/home/Home.vue';
import Bloglist  from './components/blog/Bloglist.vue';
import Blogdetails  from './components/blog/Blogdetails.vue';
import Addblog  from './components/blog/Addblog.vue';


export const routes = [ 
        
        {
            name: 'wall',
            path: "/",
            component: Home
        },
        {
            name: 'bloglist',
            path: "/bloglist",
            component: Bloglist
        },
        {
            name: 'addblog',
            path: "/addblog",
            component: Addblog
        },
        {
            name: 'blogdetails',
            path: "/blogdetails/:id",
            component: Blogdetails
        },
        {
            name: 'login',
            path: "/login",
            component: Login
        },
        {
            name: 'register',
            path: "/register",
            component: Register
        },
        {
            name: 'profile',
            path: "/profile",
            component: Profile
        },
        {
            name: 'home',
            path: "/home",
            component: Home
        }
    ];
